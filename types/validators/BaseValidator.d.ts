export = BaseValidator;
declare class BaseValidator {
    static validateWithSchema(data: any, schema: any): any;
    constructor(logger: any);
    log: any;
}
