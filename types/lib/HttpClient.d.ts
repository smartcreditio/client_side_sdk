export = HttpClient;
/**
 * Class representing the Http Client Library.
 */
declare class HttpClient {
    /**
     * Create an HTTP Client library instance.
     * @param  {Object}  [client=axios]     The http client factory.
     * @param  {Object}  [logger=log]                  The logger instance.
     */
    constructor(config: any, logger?: any, client?: any);
    config: any;
    log: any;
    client: any;
    getHttpClient(): any;
}
