export = NewBorrowingService;
declare class NewBorrowingService {
    constructor({ logger, loanValidator, web3Provider, env }: {
        logger?: any;
        loanValidator?: LoanValidator;
        web3Provider: any;
        env: any;
    });
    config: any;
    loanValidator: LoanValidator;
    web3: any;
    LOAN_CONTRACT: {
        abi: ({
            anonymous: boolean;
            inputs: {
                indexed: boolean;
                internalType: string;
                name: string;
                type: string;
            }[];
            name: string;
            type: string;
            outputs?: undefined;
            stateMutability?: undefined;
            constant?: undefined;
            payable?: undefined;
        } | {
            inputs: {
                internalType: string;
                name: string;
                type: string;
            }[];
            name: string;
            outputs: {
                internalType: string;
                name: string;
                type: string;
            }[];
            stateMutability: string;
            type: string;
            constant: boolean;
            anonymous?: undefined;
            payable?: undefined;
        } | {
            inputs: {
                components: {
                    internalType: string;
                    name: string;
                    type: string;
                }[];
                internalType: string;
                name: string;
                type: string;
            }[];
            name: string;
            outputs: {
                internalType: string;
                name: string;
                type: string;
            }[];
            stateMutability: string;
            type: string;
            constant: boolean;
            anonymous?: undefined;
            payable?: undefined;
        } | {
            inputs: ({
                components: {
                    internalType: string;
                    name: string;
                    type: string;
                }[];
                internalType: string;
                name: string;
                type: string;
            } | {
                internalType: string;
                name: string;
                type: string;
                components?: undefined;
            })[];
            name: string;
            outputs: any[];
            stateMutability: string;
            type: string;
            anonymous?: undefined;
            constant?: undefined;
            payable?: undefined;
        } | {
            inputs: ({
                components: {
                    internalType: string;
                    name: string;
                    type: string;
                }[];
                internalType: string;
                name: string;
                type: string;
            } | {
                internalType: string;
                name: string;
                type: string;
                components?: undefined;
            })[];
            name: string;
            outputs: any[];
            stateMutability: string;
            type: string;
            payable: boolean;
            anonymous?: undefined;
            constant?: undefined;
        })[];
    };
    SMARTCREDIT_ABI: {
        abi: ({
            anonymous: boolean;
            inputs: {
                indexed: boolean;
                internalType: string;
                name: string;
                type: string;
            }[];
            name: string;
            type: string;
            outputs?: undefined;
            stateMutability?: undefined;
        } | {
            inputs: ({
                internalType: string;
                name: string;
                type: string;
                components?: undefined;
            } | {
                components: {
                    internalType: string;
                    name: string;
                    type: string;
                }[];
                internalType: string;
                name: string;
                type: string;
            })[];
            name: string;
            outputs: {
                internalType: string;
                name: string;
                type: string;
            }[];
            stateMutability: string;
            type: string;
            anonymous?: undefined;
        })[];
    };
    SMARTCREDIT_CONTRACT_ADDRESS: any;
    log: any;
    client: any;
    authChallenge({ walletAddress }: {
        walletAddress: any;
    }): Promise<any>;
    getCreditLines({ token, ownerAddress, sort, limit, offset }: {
        token: any;
        ownerAddress: any;
        sort: any;
        limit: any;
        offset: any;
    }): Promise<any>;
    authUser(data: any): Promise<any>;
    getCollateral({ token, userRating, loanAmount, underlying, validity, loanTerm, collateral, }: {
        token: any;
        userRating: any;
        loanAmount: any;
        underlying: any;
        validity: any;
        loanTerm: any;
        collateral: any;
    }): Promise<any>;
    createLoan({ token, loanId, loanAmount, underlying, loanTerm, loanInterest, collateral, loanRequestValidity, userRating, borrowerAddress, signature, type, creditLineAddress, collateralAmount }: {
        token: any;
        loanId: any;
        loanAmount: any;
        underlying: any;
        loanTerm: any;
        loanInterest: any;
        collateral: any;
        loanRequestValidity: any;
        userRating: any;
        borrowerAddress: any;
        signature: any;
        type: any;
        creditLineAddress: any;
        collateralAmount: any;
    }): Promise<any>;
    getLoanDetail({ token, loanId }: {
        token: any;
        loanId: any;
    }): Promise<any>;
    getLoanList({ token, borrowerAddress, limit, order, offset }: {
        token: any;
        borrowerAddress: any;
        limit: any;
        order: any;
        offset: any;
    }): Promise<any>;
    getValidators(token: any): Promise<any>;
    terminateLoan(token: any, id: any): Promise<any>;
    liquidationProbability(token: any, loanId: any): Promise<any>;
    createCreditLine(creditLineType: any, userAddress: any): Promise<any>;
    transferEthereum(receiver: any, amount: any, userAddress: any): Promise<any>;
    transferTokens(tokenAddress: any, receiver: any, amount: any, userAddress: any): Promise<any>;
    doERC20Approval(ERC20Address: any, amount: any, spender: any, userAddress: any): Promise<any>;
    lockedAssetValue(assetAddress: any, creditLineAddress: any): Promise<any>;
    unlockedAssetValue(assetAddress: any, creditLineAddress: any): Promise<any>;
    doERC20Repayment(amount: any, loanContractAddress: any, loanData: any, userAddress: any): Promise<any>;
    doETHRepayment(amount: any, creditLineAddress: any, loanData: any, userAddress: any): Promise<any>;
    increaseLoanCollateral(amount: any, loanContractAddress: any, loanData: any, userAddress: any): Promise<any>;
    increaseLoanETHCollateral(amount: any, loanContractAddress: any, loanData: any, userAddress: any): Promise<any>;
    signLoanRequest(loanParams: any, userAddress: any): Promise<any>;
}
import LoanValidator = require("../validators/LoanValidator");
