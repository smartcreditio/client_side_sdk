export = LendingService;
declare class LendingService {
    constructor({ logger, loanValidator, web3Provider, env, }: {
        logger?: any;
        loanValidator?: LoanValidator;
        web3Provider: any;
        env: any;
    });
    config: any;
    loanValidator: LoanValidator;
    web3: any;
    SMARTCREDIT_ABI: {
        abi: ({
            anonymous: boolean;
            inputs: {
                indexed: boolean;
                internalType: string;
                name: string;
                type: string;
            }[];
            name: string;
            type: string;
            outputs?: undefined;
            stateMutability?: undefined;
        } | {
            inputs: ({
                internalType: string;
                name: string;
                type: string;
                components?: undefined;
            } | {
                components: {
                    internalType: string;
                    name: string;
                    type: string;
                }[];
                internalType: string;
                name: string;
                type: string;
            })[];
            name: string;
            outputs: {
                internalType: string;
                name: string;
                type: string;
            }[];
            stateMutability: string;
            type: string;
            anonymous?: undefined;
        })[];
    };
    SMARTCREDIT_CONTRACT_ADDRESS: any;
    log: any;
    client: any;
    authChallenge({ walletAddress }: {
        walletAddress: any;
    }): Promise<any>;
    authUser(data: any): Promise<any>;
    createFixedIncomeFund(fundType: any, fixedIncomeFundRatio: any, userAddress: any): Promise<any>;
    getFIF({ token, lenderAddress, limit, sort, offset }: {
        token: any;
        lenderAddress: any;
        limit: any;
        sort: any;
        offset: any;
    }): Promise<any>;
    depositDai(fixedIncomeFundAddress: any, amount: any, tokenAddress: any, userAddress: any): Promise<any>;
    depositEth(fixedIncomeFundAddress: any, amount: any, userAddress: any): Promise<any>;
    terminateFixedIncomeFund(fixedIncomeFundAddress: any, userAddress: any): Promise<any>;
    withdrawFixedIncomeFund(fixedIncomeFundAddress: any, amount: any, userAddress: any): Promise<any>;
}
import LoanValidator = require("../validators/LoanValidator");
