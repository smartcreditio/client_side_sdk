export = MultiValidationError;
declare class MultiValidationError extends BaseError {
    constructor(id: any, errors: any);
    id: any;
    errors: any;
}
import BaseError = require("./BaseError");
