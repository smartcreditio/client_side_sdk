export namespace beta {
    const apiUrl: string;
    const SMARTCREDIT_ADDRESS: string;
    const INSURER_ADDRESS: string;
    const signMessage: string;
    const chainID: number;
}
export namespace prod {
    const apiUrl_1: string;
    export { apiUrl_1 as apiUrl };
    const SMARTCREDIT_ADDRESS_1: string;
    export { SMARTCREDIT_ADDRESS_1 as SMARTCREDIT_ADDRESS };
    const INSURER_ADDRESS_1: string;
    export { INSURER_ADDRESS_1 as INSURER_ADDRESS };
    const signMessage_1: string;
    export { signMessage_1 as signMessage };
    const chainID_1: number;
    export { chainID_1 as chainID };
}
