module.exports = {
    extends: 'airbnb-base',
    rules: {
      indent: ['error', 2],
      'max-len': [1, 120],
      semi: [2, 'never'],
    },
  }
  