const pino = require('pino')
const serializers = require('pino-std-serializers')

const logSetting = {
  name: 'smarcredit-sdk',
  serializers,
}

const logger = pino(logSetting)

if (process.env.NODE_ENV === 'development') {
  logger.level = 'debug'
}

module.exports = logger
