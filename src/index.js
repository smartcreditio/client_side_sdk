const NewBorrowingService = require("./services/BorrowingService");
const LendingService = require("./services/LendingService");

module.exports = {
    NewBorrowingService,
    LendingService,
};