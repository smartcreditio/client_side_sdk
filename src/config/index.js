module.exports = {
  beta: {
    apiUrl: 'https://4399e4tb03.execute-api.eu-central-1.amazonaws.com/Beta',
    SMARTCREDIT_ADDRESS: "0x3371991060FBc36D6C3646A7B0Fc3FB499b457b8",
    INSURER_ADDRESS: "0xE6Ef5cAab17f31d3410780759AbF8873009Cb48c",
    signMessage: "Sign this message to authenticate with https://www.smartcredit.io. This wont cost you any Ether. Here is a uniqueu ID:",
    chainID: 3
  },
  prod: {
    apiUrl: 'https://i5jop92kw7.execute-api.eu-central-1.amazonaws.com/PROD',
    SMARTCREDIT_ADDRESS: "0x31ba589072278D82207212702De9A1C2B9D42c28",
    INSURER_ADDRESS: "0x8122435D29d509e787395cfeaC4bD46DF7104B24",
    signMessage: "Sign this message to authenticate with https://www.smartcredit.io. This wont cost you any Ether. Here is a uniqueu ID:",
    chainID: 1
  }
}