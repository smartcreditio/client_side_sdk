const log = require("../logger");
const HttpClientLib = require("../lib/HttpClient");
const LoanValidator = require("../validators/LoanValidator");
const config = require("../config");

const { SMARTCREDIT, FIXED_INCOME_FUND, ERC20 } = require("../contracts/abi")

const loanValidatorInstance = new LoanValidator(log);

class LendingService {
    constructor({
        logger = log,
        loanValidator = loanValidatorInstance,
        web3Provider,
        env,
    }) {
        if (!logger) {
            throw new TypeError(`Invalid "logger" value: ${logger}`);
        }
        if (!web3Provider) {
            throw new TypeError(`Invalid "web3" value: ${web3}`);
        }

        this.config = config[env];
        this.loanValidator = loanValidator;
        this.web3 = web3Provider;

        this.SMARTCREDIT_ABI = SMARTCREDIT;
        this.SMARTCREDIT_CONTRACT_ADDRESS = this.config.SMARTCREDIT_ADDRESS;

        this.log = logger.child({ module: this.constructor.name });
        this.client = new HttpClientLib(this.config).getHttpClient();
    }

    async authChallenge({ walletAddress }) {
        try {
            const response = await this.client.get(
                `/user/challenge?walletAddress=${walletAddress}`
            );

            return Promise.resolve(response);
        } catch (err) {
            return Promise.reject(err);
        }
    }

    async authUser(data) {
        try {
            const response = await this.client.post("/user/auth", data);

            return Promise.resolve(response);
        } catch (err) {
            return Promise.reject(err);
        }
    }

    async createFixedIncomeFund(fundType, fixedIncomeFundRatio, userAddress) {
        try {
            const salt = this.web3.utils.randomHex(32);
            const smartCreditInstance = new this.web3.eth.Contract(this.SMARTCREDIT_ABI.abi, this.SMARTCREDIT_CONTRACT_ADDRESS);

            return await smartCreditInstance
                .methods
                .createFixedIncomeFund(this.web3.utils.keccak256(fundType), salt, fixedIncomeFundRatio)
                .send({
                    from: userAddress
                })
        } catch (err) {
            return Promise.reject(err)
        }
    }

    async getFIF({ token, lenderAddress, limit, sort, offset }) {
        try {
            const response = await this.client.get(
                `/fixedincomefund/list?lenderAddress=${lenderAddress}&limit=${limit}&sort=${sort}&offset=${offset}`,
                {
                    headers: {
                        "x-token": `${token}`,
                    },
                }
            );
            return Promise.resolve(response);
        } catch (error) {
            return Promise.reject(error);
        }
    }

    async depositDai(fixedIncomeFundAddress, amount, tokenAddress, userAddress) {
        const TokenContractInstance = new this.web3.eth.Contract(ERC20.abi, tokenAddress);
        try {
            return await TokenContractInstance.methods.transfer(fixedIncomeFundAddress, parseFloat(amount).toLocaleString('fullwide', { useGrouping: false })).send({
                from: userAddress
            })
        } catch (err) {
            return Promise.reject(err)
        }
    }

    async depositEth(fixedIncomeFundAddress, amount, userAddress) {
        try {
            return await this.web3.eth.sendTransaction({
                from: userAddress,
                to: fixedIncomeFundAddress,
                value: this.web3.utils.toWei(amount.toString(), 'ether')
            })
        } catch (err) {
            return Promise.reject(err)
        }
    }

    async terminateFixedIncomeFund(fixedIncomeFundAddress, userAddress) {
        try {
            const FifContractInstance = new this.web3.eth.Contract(FIXED_INCOME_FUND.abi, fixedIncomeFundAddress);
            return await FifContractInstance.methods.terminate().send({
                from: userAddress
            })
        } catch (err) {
            console.log(err)
            return Promise.reject(err)
        }
    }

   async withdrawFixedIncomeFund(fixedIncomeFundAddress, amount, userAddress) {
        try {
            const FifContractInstance = new this.web3.eth.Contract(FIXED_INCOME_FUND.abi, fixedIncomeFundAddress);
            return await FifContractInstance.methods.withdraw(amount).send({
                from: userAddress
            })
        } catch (err) {
            return Promise.reject(err)
        }
    }
}

module.exports = LendingService;