const { default: BigNumber } = require('bignumber.js');

const config = require("../config");
const HttpClientLib = require("../lib/HttpClient");
const LoanValidator = require("../validators/LoanValidator");
const log = require("../logger");

const { SMARTCREDIT, FIXED_INCOME_FUND, ERC20, LOAN_CONTRACT } = require("../contracts/abi")

const loanValidatorInstance = new LoanValidator(log);

class NewBorrowingService {
  constructor({
    logger = log,
    loanValidator = loanValidatorInstance,
    web3Provider,
    env
  }) {
    if (!logger) {
      throw new TypeError(`Invalid "logger" value: ${logger}`);
    }
    if (!web3Provider) {
      throw new TypeError(`Invalid "web3" value: ${web3}`);
    }

    this.config = config[env];
    this.loanValidator = loanValidator;
    this.web3 = web3Provider;

    this.LOAN_CONTRACT = LOAN_CONTRACT;
    this.SMARTCREDIT_ABI = SMARTCREDIT;
    this.SMARTCREDIT_CONTRACT_ADDRESS = this.config.SMARTCREDIT_ADDRESS;

    this.log = logger.child({ module: this.constructor.name });
    this.client = new HttpClientLib(this.config).getHttpClient();

  }

  // BACKEND CALLS 


  async authChallenge({ walletAddress }) {
    try {
      const response = await this.client.get(
        `/user/challenge?walletAddress=${walletAddress}`
      );

      return Promise.resolve(response);
    } catch (err) {
      return Promise.reject(err);
    }
  }

  async getCreditLines({ token, ownerAddress, sort, limit, offset }) {
    try {
      const response = await this.client.get(
        `/loan/creditline?ownerAddress=${ownerAddress}&sort=${sort}&limit=${limit}&offset=${offset}`,
        {
          headers: {
            "x-token": `${token}`,
          },
        }
      );
      return Promise.resolve(response);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async authUser(data) {
    try {
      const response = await this.client.post("/user/auth", data);

      return Promise.resolve(response);
    } catch (err) {
      return Promise.reject(err);
    }
  }

  async getCollateral({
    token,
    userRating,
    loanAmount,
    underlying,
    validity,
    loanTerm,
    collateral,
  }) {
    try {
      const response = await this.client.get(
        `/loan/collateral?userRating=${userRating}&loanAmount=${loanAmount}&underlying=${underlying}&validity=${validity}&loanTerm=${loanTerm}&collateral=${collateral}`,
        {
          headers: {
            "x-token": `${token}`,
          },
        }
      );

      return Promise.resolve(response);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async createLoan({
    token,
    loanId,
    loanAmount,
    underlying,
    loanTerm,
    loanInterest,
    collateral,
    loanRequestValidity,
    userRating,
    borrowerAddress,
    signature,
    type,
    creditLineAddress,
    collateralAmount
  }) {
    try {
      const response = await this.client.post(
        "/loan/create",
        {
          token,
          loanId,
          loanAmount,
          underlying,
          loanTerm,
          loanInterest,
          collateral,
          loanRequestValidity,
          userRating,
          borrowerAddress,
          signature,
          type,
          creditLineAddress,
          collateralAmount
        },
        {
          headers: {
            "x-token": `${token}`,
          },
        }
      );
      return Promise.resolve(response);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async getLoanDetail({ token, loanId }) {
    try {
      const response = await this.client.get(`/loan/details?loanId=${loanId}`, {
        headers: {
          "x-token": `${token}`,
        },
      });

      return Promise.resolve(response);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async getLoanList({ token, borrowerAddress, limit, order, offset }) {
    try {
      const response = await this.client.get(
        `/loan/list?borrowerAddress=${borrowerAddress}&limit=${limit}&order=${order}&offset=${offset}`,
        {
          headers: {
            "x-token": `${token}`,
          },
        }
      );
      return Promise.resolve(response);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async getValidators(token) {
    try {
      const response = await this.client.get("/loan/validators", {
        headers: {
          "x-token": `${token}`,
        },
      });

      return Promise.resolve(response);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async terminateLoan(token, id) {
    try {
      const response = await this.client.post("/loan/terminate", { token, id }, {
        headers: {
          "x-token": `${token}`,
        },
      });

      return Promise.resolve(response);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async liquidationProbability( token, loanId ) {
    try {
      const response = await this.client.get(
        `/loan/liquidation?loanId=${loanId}`,
        {
          headers: {
            "x-token": `${token}`,
          },
        }
      );
      return Promise.resolve(response);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  // SMARTCONTRACT CALLS

  // Create Credit Line
  async createCreditLine(creditLineType, userAddress) {
    try {
      const salt = this.web3.utils.randomHex(32);
      const smartCreditInstance = new this.web3.eth.Contract(SMARTCREDIT.abi, this.SMARTCREDIT_CONTRACT_ADDRESS);
      return await smartCreditInstance.methods
        .createCreditLine(this.web3.utils.keccak256(creditLineType), salt)
        .send({
          from: userAddress
        })
    } catch (err) {
      return Promise.reject(err)
    }
  }

  // Deposit EThereum
  async transferEthereum(receiver, amount, userAddress) {
    try {
      return await this.web3.eth.sendTransaction({
        from: userAddress,
        to: receiver, value: parseFloat(amount).toLocaleString('fullwide', { useGrouping: false })
      })
    } catch (err) {
      return Promise.reject(err)
    }
  }

  // Deposit ERC20
  async transferTokens(tokenAddress, receiver, amount, userAddress) {
    try {
      const TokenContractInstance = new this.web3.eth.Contract(ERC20.abi, tokenAddress);
      return await TokenContractInstance.methods.transfer(receiver, parseFloat(amount).toLocaleString('fullwide', { useGrouping: false })).send({
        from: userAddress
      })
    } catch (err) {
      return Promise.reject(err)
    }
  }

  // Approve ERC20 before making a repayment
  async doERC20Approval(ERC20Address, amount, spender, userAddress) {
    try {
      const contractInstance = new this.web3.eth.Contract(ERC20.abi, ERC20Address);
      return await contractInstance.methods.approve(spender, amount.toString()).send({
        from: userAddress
      })

    } catch (err) {
      return Promise.reject(err)
    }
  }


  async lockedAssetValue(assetAddress, creditLineAddress) {
    try {
      const LoanContractInstance = new this.web3.eth.Contract(LOAN_CONTRACT.abi, creditLineAddress);
      return await LoanContractInstance.methods.getLockedAssetValue(assetAddress).call();
    } catch (err) {
      return Promise.reject(err)
    }
  }

  async unlockedAssetValue(assetAddress, creditLineAddress) {
    try {
      const LoanContractInstance = new this.web3.eth.Contract(LOAN_CONTRACT.abi, creditLineAddress);
      return await LoanContractInstance.methods.getUnlockedAssetValue(assetAddress).call();
    } catch (err) {
      return Promise.reject(err)
    }
  }

  async doERC20Repayment(amount, loanContractAddress, loanData, userAddress) {
    try {
      const LoanContractInstance = new this.web3.eth.Contract(LOAN_CONTRACT.abi, loanContractAddress);
      const insurerAddress = this.config.INSURER_ADDRESS;

      return await LoanContractInstance.methods.repay(
        [
          loanData.id,
          parseFloat((loanData.netLoanInterestRate * 100).toFixed(0)),
          loanData.loanTerm,
          loanData.loanAmount.toString(),
          parseFloat(((loanData.loanInsurancePremium + loanData.platformFee) * 100).toFixed(0)),
          loanData.collateralInfo.amount.toString(),
          loanData.lenderAddress,
          loanData.collateral.ethAddress,
          loanData.underlying.ethAddress,
          insurerAddress
        ],
        amount
      ).send({
        from: userAddress
      })

    } catch (err) {
      return Promise.reject(err)

    }
  }

  async doETHRepayment(amount, creditLineAddress, loanData, userAddress) {
    try {
      const LoanContractInstance = new this.web3.eth.Contract(LOAN_CONTRACT.abi, creditLineAddress);
      const insurerAddress = this.config.INSURER_ADDRESS;
      return await LoanContractInstance.methods.repay(
        [
          loanData.id,
          parseFloat((loanData.netLoanInterestRate * 100).toFixed(0)),
          loanData.loanTerm,
          loanData.loanAmount.toString(),
          parseFloat(((loanData.loanInsurancePremium + loanData.platformFee) * 100).toFixed(0)),
          loanData.collateralInfo.amount.toLocaleString('fullwide', { useGrouping: false }),
          loanData.lenderAddress,
          loanData.collateral.ethAddress,
          loanData.underlying.ethAddress,
          insurerAddress
        ],
        amount
      ).send({
        from: userAddress,
        value: amount
      })

    } catch (err) {
      return Promise.reject(err)

    }
  }

  async increaseLoanCollateral(amount, loanContractAddress, loanData, userAddress) {
    try {
      const LoanContractInstance = new this.web3.eth.Contract(LOAN_CONTRACT.abi, loanContractAddress);
      const insurerAddress = this.config.INSURER_ADDRESS;

      return await LoanContractInstance.methods.increaseCollateral(
        [
          loanData.id,
          parseFloat((loanData.netLoanInterestRate * 100).toFixed(0)),
          loanData.loanTerm,
          loanData.loanAmount.toLocaleString('fullwide', { useGrouping: false }),
          parseFloat(((loanData.loanInsurancePremium + loanData.platformFee) * 100).toFixed(0)),
          loanData.collateralInfo.amount.toLocaleString('fullwide', { useGrouping: false }),
          loanData.lenderAddress,
          loanData.collateral.ethAddress,
          loanData.underlying.ethAddress,
          insurerAddress
        ],
        parseFloat(amount).toFixed(0)
      ).send({
        from: userAddress
      })
    } catch (err) {
      return Promise.reject(err)
    }
  }

  async increaseLoanETHCollateral(amount, loanContractAddress, loanData, userAddress) {
    try {
      const LoanContractInstance = new this.web3.eth.Contract(LOAN_CONTRACT.abi, loanContractAddress);
      const insurerAddress = this.config.INSURER_ADDRESS;

      return await LoanContractInstance.methods.increaseCollateral(
        [
          loanData.id,
          parseFloat((loanData.netLoanInterestRate * 100).toFixed(0)),
          loanData.loanTerm,
          loanData.loanAmount.toLocaleString('fullwide', { useGrouping: false }),
          parseFloat(((loanData.loanInsurancePremium + loanData.platformFee) * 100).toFixed(0)),
          loanData.collateralInfo.amount.toLocaleString('fullwide', { useGrouping: false }),
          loanData.lenderAddress,
          loanData.collateral.ethAddress,
          loanData.underlying.ethAddress,
          insurerAddress
        ],
        parseFloat(amount).toFixed(0)
      ).send({
        from: userAddress,
        value:amount
      })
    } catch (err) {
      return Promise.reject(err)
    }
  }


  async signLoanRequest(loanParams, userAddress) {
    const messageParams = JSON.stringify({
      domain: {
        name: 'Credit Line',
        chainId: this.config.chainID,
        version: '1.0.0',
        verifyingContract: loanParams.creditLineAddress
      },
      message: {
        loanId: loanParams.loanId,
        loanInterestRate: parseFloat((loanParams.loanRate * 100).toFixed(0)),
        loanTerm: loanParams.loanTerm,
        loanAmount: convertFloatToInteger(loanParams.loanAmount, loanParams.underlyingDecimalPlaces).toString(),
        fee: parseFloat((loanParams.loanFee * 100).toFixed(0)),
        initialCollateralAmount: convertFloatToInteger(loanParams.collateralAmount, loanParams.collateralDecimalPlaces).toString(),
        collateralAddress: loanParams.collateralCurrency,
        underlyingAddress: loanParams.underlyingCurrency
      },
      primaryType: 'LoanRequest',
      types: {
        EIP712Domain: [
          { name: 'name', type: 'string' },
          { name: 'version', type: 'string' },
          { name: 'chainId', type: 'uint256' },
          { name: 'verifyingContract', type: 'address' }
        ],
        LoanRequest: [
          { name: 'loanId', type: 'bytes32' },
          { name: 'loanInterestRate', type: 'uint64' },
          { name: 'loanTerm', type: 'uint64' },
          { name: 'loanAmount', type: 'uint256' },
          { name: 'fee', type: 'uint256' },
          { name: 'initialCollateralAmount', type: 'uint256' },
          { name: 'collateralAddress', type: 'address' },
          { name: 'underlyingAddress', type: 'address' }
        ]
      }
    });

    const from = userAddress;
    const params = [from, messageParams];
    const method = 'eth_signTypedData_v4';

    return new Promise(async (resolve, reject) => {
      this.web3.currentProvider.sendAsync({ method, params, from },
        (err, res) => {
          if (err) {
            reject(err);
          }
          if (res.error) {
            reject(res.error);
          }
          if (res) {
            resolve(res.result);
          }
        });
    });
  }
}

const convertFloatToInteger = (value, decimalPlaces) => {
  return new BigNumber(value).times(10 ** decimalPlaces).toFixed(0)
}

const convertIntegerToFloat = (value, decimalPlaces) => {
  return new BigNumber(value).div(10 ** decimalPlaces).toFixed(6)
}

module.exports = NewBorrowingService;
