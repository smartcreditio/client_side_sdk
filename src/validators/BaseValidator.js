const { ValidationError, MultiValidationError } = require('../utils/errors')

//  Class representing a base validator.

class BaseValidator {
//   /
//    * Create a base validator.Base
//    * @param  {Object}    logger The logger instance.
//    * @throws {TypeError}        If some required property is missing.
//    */
  constructor(logger) {
    if (!logger) {
      throw new TypeError(`Invalid "logger" value: ${logger}`)
    }
    this.log = logger.child({ module: this.constructor.name })
  }

  static validateWithSchema(data, schema) {
    let errors
    const validatorOptions = {
      abortEarly: false,
    }
    const { error, value } = schema.validate(data, validatorOptions)

    if (error && error.details) {
      errors = error.details.map((item) => new ValidationError(item.message, item.context.key))
    }

    if (errors) {
      throw new MultiValidationError((values && values.id) || '', errors)
    }

    return value
  }
}

module.exports = BaseValidator
