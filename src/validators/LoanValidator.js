const Joi = require('joi')
const BaseValidator = require('./BaseValidator')

class LoanValidator extends BaseValidator {
  createLoan(data) {
    const createLoanSchema = Joi.object({
      token: Joi.string().required(),
      loanAmount: Joi.number().required(),
      underlying: Joi.string().required(),
      loanTerm: Joi.number().required(),
      loanInterest: Joi.number().required(),
      collateral: Joi.string().required(),
      loanRequestValidity: Joi.date().required(),
      userRating: Joi.number().required(),
      borrowerAddress: Joi.string().required().max(42),
    })
    return BaseValidator.validateWithSchema(data, createLoanSchema)
  }
}

module.exports = LoanValidator
