exports.SMARTCREDIT = {
  abi: [{
    anonymous: false,
    inputs: [{
      indexed: true,
      internalType: 'address',
      name: 'fixedIncomeFund',
      type: 'address'
    },
    {
      indexed: true,
      internalType: 'bytes32',
      name: 'salt',
      type: 'bytes32'
    }
    ],
    name: 'FixedIncomeFundCreationComplete',
    type: 'event'
  }, {
    anonymous: false,
    inputs: [{
      indexed: true,
      internalType: 'address',
      name: 'creditLine',
      type: 'address'
    },
    {
      indexed: true,
      internalType: 'address',
      name: 'borrower',
      type: 'address'
    },
    {
      indexed: false,
      internalType: 'bytes32',
      name: 'salt',
      type: 'bytes32'
    },
    {
      indexed: false,
      internalType: 'bytes32',
      name: '_type',
      type: 'bytes32'
    }
    ],
    name: 'LoanContractCreated',
    type: 'event'
  }, {
    inputs: [{
      internalType: 'bytes32',
      name: '_type',
      type: 'bytes32'
    },
    {
      internalType: 'bytes32',
      name: '_salt',
      type: 'bytes32'
    }
    ],
    name: 'createCreditLine',
    outputs: [{
      internalType: 'address',
      name: '_creditLine',
      type: 'address'
    }],
    stateMutability: 'nonpayable',
    type: 'function'
  }, {
    inputs: [{
      internalType: 'bytes32',
      name: '_type',
      type: 'bytes32'
    },
    {
      internalType: 'bytes32',
      name: '_salt',
      type: 'bytes32'
    },
    {
      components: [{
        internalType: 'address',
        name: 'assetAddress',
        type: 'address'
      },
      {
        internalType: 'uint256',
        name: 'loanAmount',
        type: 'uint256'
      },
      {
        internalType: 'uint256',
        name: 'loanTerm',
        type: 'uint256'
      },
      {
        internalType: 'uint256',
        name: 'interestRate',
        type: 'uint256'
      },
      {
        internalType: 'address',
        name: 'collateralAddress',
        type: 'address'
      },
      {
        internalType: 'uint256',
        name: 'collateralId',
        type: 'uint256'
      }
      ],
      internalType: 'struct INFTLoan.LoanRequestCreate',
      name: '_loanRequest',
      type: 'tuple'
    }
    ],
    name: 'createNFTLoan',
    outputs: [{
      internalType: 'address',
      name: '_loanContract',
      type: 'address'
    }],
    stateMutability: 'nonpayable',
    type: 'function'
  }, {
    inputs: [{
      internalType: 'bytes32',
      name: '_type',
      type: 'bytes32'
    },
    {
      internalType: 'bytes32',
      name: '_salt',
      type: 'bytes32'
    },
    {
      internalType: 'uint256[4]',
      name: '_ratios',
      type: 'uint256[4]'
    }
    ],
    name: 'createFixedIncomeFund',
    outputs: [{
      internalType: 'address',
      name: '_fixedIncomeFund',
      type: 'address'
    }],
    stateMutability: 'nonpayable',
    type: 'function'
  }, {
    inputs: [{
      internalType: 'address[]',
      name: '_fixedIncomeFunds',
      type: 'address[]'
    }],
    name: 'investFixedIncomeFundToCompound',
    outputs: [],
    stateMutability: 'nonpayable',
    type: 'function'
  }]
};

exports.FIXED_INCOME_FUND = {
  abi: [{
    anonymous: false,
    inputs: [{
      indexed: false,
      internalType: 'uint256',
      name: '',
      type: 'uint256'
    },
    {
      indexed: false,
      internalType: 'uint256',
      name: '',
      type: 'uint256'
    },
    {
      indexed: false,
      internalType: 'uint256',
      name: '',
      type: 'uint256'
    },
    {
      indexed: false,
      internalType: 'uint256',
      name: '',
      type: 'uint256'
    }
    ],
    name: 'FixedIncomeFundRatiosUpdated',
    type: 'event'
  },
  {
    anonymous: false,
    inputs: [{
      indexed: false,
      internalType: 'address',
      name: '_fundOwner',
      type: 'address'
    }],
    name: 'FixedIncomeFundTerminated',
    type: 'event'
  },
  {
    anonymous: false,
    inputs: [{
      indexed: false,
      internalType: 'uint256',
      name: '_amount',
      type: 'uint256'
    },
    {
      indexed: false,
      internalType: 'address',
      name: '_withdrawer',
      type: 'address'
    }
    ],
    name: 'Withdrawn',
    type: 'event'
  },
  {
    inputs: [{
      internalType: 'address',
      name: 'creditLine',
      type: 'address'
    }],
    name: 'checkCreditLineAddress',
    outputs: [{
      internalType: 'bool',
      name: '',
      type: 'bool'
    }],
    stateMutability: 'view',
    type: 'function',
    constant: true
  },
  {
    inputs: [{
      internalType: 'uint8',
      name: '_position',
      type: 'uint8'
    }],
    name: 'getBucketInfo',
    outputs: [{
      internalType: 'uint256',
      name: '',
      type: 'uint256'
    },
    {
      internalType: 'uint256',
      name: '',
      type: 'uint256'
    }
    ],
    stateMutability: 'view',
    type: 'function',
    constant: true
  },
  {
    inputs: [{
      internalType: 'uint8',
      name: '_position',
      type: 'uint8'
    }],
    name: 'getBucketInvested',
    outputs: [{
      internalType: 'uint256',
      name: '',
      type: 'uint256'
    }],
    stateMutability: 'view',
    type: 'function',
    constant: true
  },
  {
    inputs: [{
      internalType: 'uint256',
      name: '_loanTerm',
      type: 'uint256'
    }],
    name: 'getBucketPosition',
    outputs: [{
      internalType: 'uint8',
      name: '',
      type: 'uint8'
    }],
    stateMutability: 'pure',
    type: 'function',
    constant: true
  },
  {
    inputs: [],
    name: 'getFixedIncomeFundDetails',
    outputs: [{
      internalType: 'address',
      name: '',
      type: 'address'
    },
    {
      internalType: 'address',
      name: '',
      type: 'address'
    },
    {
      internalType: 'uint256',
      name: '',
      type: 'uint256'
    },
    {
      internalType: 'uint256',
      name: '',
      type: 'uint256'
    },
    {
      internalType: 'uint256[4]',
      name: '',
      type: 'uint256[4]'
    }
    ],
    stateMutability: 'view',
    type: 'function',
    constant: true
  },
  {
    inputs: [],
    name: 'getFundOwner',
    outputs: [{
      internalType: 'address',
      name: '',
      type: 'address'
    }],
    stateMutability: 'view',
    type: 'function',
    constant: true
  },
  {
    inputs: [],
    name: 'totalFundInvested',
    outputs: [{
      internalType: 'uint256',
      name: '_totalInvested',
      type: 'uint256'
    }],
    stateMutability: 'view',
    type: 'function',
    constant: true
  },
  {
    inputs: [{
      internalType: 'uint256[4]',
      name: '_ratios',
      type: 'uint256[4]'
    }],
    name: 'updateBucketRatio',
    outputs: [],
    stateMutability: 'nonpayable',
    type: 'function'
  },
  {
    inputs: [{
      internalType: 'uint256',
      name: '_loanAmount',
      type: 'uint256'
    },
    {
      internalType: 'uint8',
      name: '_bucketPosition',
      type: 'uint8'
    }
    ],
    name: 'validateInvestmentRequest',
    outputs: [{
      internalType: 'bool',
      name: '',
      type: 'bool'
    }],
    stateMutability: 'view',
    type: 'function',
    constant: true
  },
  {
    inputs: [{
      internalType: 'uint256',
      name: '_amount',
      type: 'uint256'
    }],
    name: 'withdraw',
    outputs: [{
      internalType: 'bool',
      name: '',
      type: 'bool'
    }],
    stateMutability: 'nonpayable',
    type: 'function'
  },
  {
    stateMutability: 'payable',
    type: 'receive',
    payable: true
  },
  {
    inputs: [],
    name: 'terminate',
    outputs: [],
    stateMutability: 'nonpayable',
    type: 'function'
  },
  {
    inputs: [],
    name: 'getCurrencyAddress',
    outputs: [{
      internalType: 'address',
      name: '',
      type: 'address'
    }],
    stateMutability: 'view',
    type: 'function',
    constant: true
  },
  {
    inputs: [],
    name: 'getCompoundAddress',
    outputs: [{
      internalType: 'address',
      name: '',
      type: 'address'
    }],
    stateMutability: 'pure',
    type: 'function',
    constant: true
  },
  {
    inputs: [],
    name: 'fixedIncomeFundBalance',
    outputs: [{
      internalType: 'uint256',
      name: '',
      type: 'uint256'
    }],
    stateMutability: 'view',
    type: 'function',
    constant: true
  }
  ]
};

exports.ERC20 = {
  address: '',
  abi: [
    {
      constant: true,
      inputs: [],
      name: 'name',
      outputs: [
        {
          name: '',
          type: 'string'
        }
      ],
      payable: false,
      stateMutability: 'view',
      type: 'function'
    },
    {
      constant: false,
      inputs: [
        {
          name: '_spender',
          type: 'address'
        },
        {
          name: '_value',
          type: 'uint256'
        }
      ],
      name: 'approve',
      outputs: [
        {
          name: '',
          type: 'bool'
        }
      ],
      payable: false,
      stateMutability: 'nonpayable',
      type: 'function'
    },
    {
      constant: true,
      inputs: [],
      name: 'totalSupply',
      outputs: [
        {
          name: '',
          type: 'uint256'
        }
      ],
      payable: false,
      stateMutability: 'view',
      type: 'function'
    },
    {
      constant: false,
      inputs: [
        {
          name: '_from',
          type: 'address'
        },
        {
          name: '_to',
          type: 'address'
        },
        {
          name: '_value',
          type: 'uint256'
        }
      ],
      name: 'transferFrom',
      outputs: [
        {
          name: '',
          type: 'bool'
        }
      ],
      payable: false,
      stateMutability: 'nonpayable',
      type: 'function'
    },
    {
      constant: true,
      inputs: [],
      name: 'decimals',
      outputs: [
        {
          name: '',
          type: 'uint8'
        }
      ],
      payable: false,
      stateMutability: 'view',
      type: 'function'
    },
    {
      constant: false,
      inputs: [
        {
          name: '_spender',
          type: 'address'
        },
        {
          name: '_subtractedValue',
          type: 'uint256'
        }
      ],
      name: 'decreaseApproval',
      outputs: [
        {
          name: '',
          type: 'bool'
        }
      ],
      payable: false,
      stateMutability: 'nonpayable',
      type: 'function'
    },
    {
      constant: true,
      inputs: [
        {
          name: '_owner',
          type: 'address'
        }
      ],
      name: 'balanceOf',
      outputs: [
        {
          name: 'balance',
          type: 'uint256'
        }
      ],
      payable: false,
      stateMutability: 'view',
      type: 'function'
    },
    {
      constant: true,
      inputs: [],
      name: 'symbol',
      outputs: [
        {
          name: '',
          type: 'string'
        }
      ],
      payable: false,
      stateMutability: 'view',
      type: 'function'
    },
    {
      constant: false,
      inputs: [
        {
          name: '_to',
          type: 'address'
        },
        {
          name: '_value',
          type: 'uint256'
        }
      ],
      name: 'transfer',
      outputs: [
        {
          name: '',
          type: 'bool'
        }
      ],
      payable: false,
      stateMutability: 'nonpayable',
      type: 'function'
    },
    {
      constant: false,
      inputs: [
        {
          name: '_spender',
          type: 'address'
        },
        {
          name: '_addedValue',
          type: 'uint256'
        }
      ],
      name: 'increaseApproval',
      outputs: [
        {
          name: '',
          type: 'bool'
        }
      ],
      payable: false,
      stateMutability: 'nonpayable',
      type: 'function'
    },
    {
      constant: true,
      inputs: [
        {
          name: '_owner',
          type: 'address'
        },
        {
          name: '_spender',
          type: 'address'
        }
      ],
      name: 'allowance',
      outputs: [
        {
          name: '',
          type: 'uint256'
        }
      ],
      payable: false,
      stateMutability: 'view',
      type: 'function'
    },
    {
      inputs: [
        {
          name: 'name',
          type: 'string'
        },
        {
          name: 'symbol',
          type: 'string'
        },
        {
          name: 'decimals',
          type: 'uint8'
        },
        {
          name: 'totalSupply',
          type: 'uint256'
        }
      ],
      payable: false,
      stateMutability: 'nonpayable',
      type: 'constructor'
    },
    {
      anonymous: false,
      inputs: [
        {
          indexed: true,
          name: 'from',
          type: 'address'
        },
        {
          indexed: true,
          name: 'to',
          type: 'address'
        },
        {
          indexed: false,
          name: 'value',
          type: 'uint256'
        }
      ],
      name: 'Transfer',
      type: 'event'
    },
    {
      anonymous: false,
      inputs: [
        {
          indexed: true,
          name: 'owner',
          type: 'address'
        },
        {
          indexed: true,
          name: 'spender',
          type: 'address'
        },
        {
          indexed: false,
          name: 'value',
          type: 'uint256'
        }
      ],
      name: 'Approval',
      type: 'event'
    }
  ]
};

exports.LOAN_CONTRACT = {
  abi: [
    {
      anonymous: false,
      inputs: [
        {
          indexed: true,
          internalType: 'bytes32',
          name: 'loanId',
          type: 'bytes32'
        },
        {
          indexed: false,
          internalType: 'address',
          name: 'sender',
          type: 'address'
        },
        {
          indexed: false,
          internalType: 'uint256',
          name: 'amount',
          type: 'uint256'
        }
      ],
      name: 'CollateralIncreased',
      type: 'event'
    },
    {
      anonymous: false,
      inputs: [
        {
          indexed: true,
          internalType: 'bytes32',
          name: 'loanId',
          type: 'bytes32'
        },
        {
          indexed: false,
          internalType: 'address',
          name: 'exchangeAddress',
          type: 'address'
        },
        {
          indexed: false,
          internalType: 'uint256',
          name: 'loanStatus',
          type: 'uint256'
        }
      ],
      name: 'LiquidationInitiated',
      type: 'event'
    },
    {
      anonymous: false,
      inputs: [
        {
          indexed: true,
          internalType: 'bytes32',
          name: 'loanId',
          type: 'bytes32'
        },
        {
          indexed: false,
          internalType: 'uint256',
          name: 'loanAmount',
          type: 'uint256'
        },
        {
          indexed: false,
          internalType: 'uint64',
          name: 'interestRate',
          type: 'uint64'
        },
        {
          indexed: false,
          internalType: 'uint64',
          name: 'duration',
          type: 'uint64'
        },
        {
          indexed: false,
          internalType: 'address',
          name: 'collateralAddress',
          type: 'address'
        },
        {
          indexed: false,
          internalType: 'address',
          name: 'underlyingAddress',
          type: 'address'
        },
        {
          indexed: false,
          internalType: 'address',
          name: 'lender',
          type: 'address'
        }
      ],
      name: 'LoanInitialized',
      type: 'event'
    },
    {
      anonymous: false,
      inputs: [
        {
          indexed: true,
          internalType: 'bytes32',
          name: 'loanId',
          type: 'bytes32'
        },
        {
          indexed: false,
          internalType: 'uint256',
          name: 'amount',
          type: 'uint256'
        },
        {
          indexed: false,
          internalType: 'uint256',
          name: 'outstandingAmount',
          type: 'uint256'
        },
        {
          indexed: false,
          internalType: 'uint256',
          name: 'timestamp',
          type: 'uint256'
        }
      ],
      name: 'Repaid',
      type: 'event'
    },
    {
      inputs: [],
      name: 'getBorrower',
      outputs: [
        {
          internalType: 'address',
          name: '',
          type: 'address'
        }
      ],
      stateMutability: 'view',
      type: 'function',
      constant: true
    },
    {
      inputs: [
        {
          internalType: 'address',
          name: '_assetAddress',
          type: 'address'
        }
      ],
      name: 'getERC20Balance',
      outputs: [
        {
          internalType: 'uint256',
          name: '',
          type: 'uint256'
        }
      ],
      stateMutability: 'view',
      type: 'function',
      constant: true
    },
    {
      inputs: [],
      name: 'getETHBalance',
      outputs: [
        {
          internalType: 'uint256',
          name: '',
          type: 'uint256'
        }
      ],
      stateMutability: 'view',
      type: 'function',
      constant: true
    },
    {
      inputs: [
        {
          components: [
            {
              internalType: 'bytes32',
              name: 'loanId',
              type: 'bytes32'
            },
            {
              internalType: 'uint64',
              name: 'loanInterestRate',
              type: 'uint64'
            },
            {
              internalType: 'uint64',
              name: 'loanTerm',
              type: 'uint64'
            },
            {
              internalType: 'uint256',
              name: 'loanAmount',
              type: 'uint256'
            },
            {
              internalType: 'uint256',
              name: 'fee',
              type: 'uint256'
            },
            {
              internalType: 'uint256',
              name: 'initialCollateralAmount',
              type: 'uint256'
            },
            {
              internalType: 'address',
              name: 'lender',
              type: 'address'
            },
            {
              internalType: 'address',
              name: 'collateralAddress',
              type: 'address'
            },
            {
              internalType: 'address',
              name: 'underlyingAddress',
              type: 'address'
            },
            {
              internalType: 'address',
              name: 'insurer',
              type: 'address'
            }
          ],
          internalType: 'struct LoanStorage.LoanRequest',
          name: '_loanRequest',
          type: 'tuple'
        }
      ],
      name: 'getHash',
      outputs: [
        {
          internalType: 'bytes32',
          name: '',
          type: 'bytes32'
        }
      ],
      stateMutability: 'pure',
      type: 'function',
      constant: true
    },
    {
      inputs: [
        {
          internalType: 'bytes32',
          name: '_loanId',
          type: 'bytes32'
        }
      ],
      name: 'getLoanData',
      outputs: [
        {
          internalType: 'bytes32',
          name: 'loanId',
          type: 'bytes32'
        },
        {
          internalType: 'uint256',
          name: 'currentCollateralAmount',
          type: 'uint256'
        },
        {
          internalType: 'uint256',
          name: 'loanEnded',
          type: 'uint256'
        },
        {
          internalType: 'uint256',
          name: 'outstandingAmount',
          type: 'uint256'
        }
      ],
      stateMutability: 'view',
      type: 'function',
      constant: true
    },
    {
      inputs: [
        {
          internalType: 'address',
          name: '_assetAddress',
          type: 'address'
        }
      ],
      name: 'getLockedAssetValue',
      outputs: [
        {
          internalType: 'uint256',
          name: '',
          type: 'uint256'
        }
      ],
      stateMutability: 'view',
      type: 'function',
      constant: true
    },
    {
      inputs: [
        {
          internalType: 'address',
          name: '_assetAddress',
          type: 'address'
        }
      ],
      name: 'getUnlockedAssetValue',
      outputs: [
        {
          internalType: 'uint256',
          name: '',
          type: 'uint256'
        }
      ],
      stateMutability: 'view',
      type: 'function',
      constant: true
    },
    {
      inputs: [
        {
          components: [
            {
              internalType: 'bytes32',
              name: 'loanId',
              type: 'bytes32'
            },
            {
              internalType: 'uint64',
              name: 'loanInterestRate',
              type: 'uint64'
            },
            {
              internalType: 'uint64',
              name: 'loanTerm',
              type: 'uint64'
            },
            {
              internalType: 'uint256',
              name: 'loanAmount',
              type: 'uint256'
            },
            {
              internalType: 'uint256',
              name: 'fee',
              type: 'uint256'
            },
            {
              internalType: 'uint256',
              name: 'initialCollateralAmount',
              type: 'uint256'
            },
            {
              internalType: 'address',
              name: 'lender',
              type: 'address'
            },
            {
              internalType: 'address',
              name: 'collateralAddress',
              type: 'address'
            },
            {
              internalType: 'address',
              name: 'underlyingAddress',
              type: 'address'
            },
            {
              internalType: 'address',
              name: 'insurer',
              type: 'address'
            }
          ],
          internalType: 'struct LoanStorage.LoanRequest',
          name: '_loanRequest',
          type: 'tuple'
        },
        {
          internalType: 'uint256',
          name: '_collateralAmount',
          type: 'uint256'
        }
      ],
      name: 'increaseCollateral',
      outputs: [],
      stateMutability: 'nonpayable',
      type: 'function'
    },
    {
      inputs: [
        {
          internalType: 'address',
          name: '_assetAddress',
          type: 'address'
        },
        {
          internalType: 'uint256',
          name: '_amount',
          type: 'uint256'
        }
      ],
      name: 'withdrawAsset',
      outputs: [],
      stateMutability: 'nonpayable',
      type: 'function'
    },
    {
      inputs: [
        {
          components: [
            {
              internalType: 'bytes32',
              name: 'loanId',
              type: 'bytes32'
            },
            {
              internalType: 'uint64',
              name: 'loanInterestRate',
              type: 'uint64'
            },
            {
              internalType: 'uint64',
              name: 'loanTerm',
              type: 'uint64'
            },
            {
              internalType: 'uint256',
              name: 'loanAmount',
              type: 'uint256'
            },
            {
              internalType: 'uint256',
              name: 'fee',
              type: 'uint256'
            },
            {
              internalType: 'uint256',
              name: 'initialCollateralAmount',
              type: 'uint256'
            },
            {
              internalType: 'address',
              name: 'lender',
              type: 'address'
            },
            {
              internalType: 'address',
              name: 'collateralAddress',
              type: 'address'
            },
            {
              internalType: 'address',
              name: 'underlyingAddress',
              type: 'address'
            },
            {
              internalType: 'address',
              name: 'insurer',
              type: 'address'
            }
          ],
          internalType: 'struct LoanStorage.LoanRequest',
          name: '_loanRequest',
          type: 'tuple'
        },
        {
          internalType: 'uint256',
          name: '_amount',
          type: 'uint256'
        }
      ],
      name: 'repay',
      outputs: [],
      stateMutability: 'payable',
      type: 'function',
      payable: true
    }
  ]
};