const axios = require('axios')
const log = require('../logger')
/**
 * Class representing the Http Client Library.
 */

class HttpClient {
  /**
   * Create an HTTP Client library instance.
   * @param  {Object}  [client=axios]     The http client factory.
   * @param  {Object}  [logger=log]                  The logger instance.
   */
  constructor(config, logger = log, client = axios) {
  
    if (!config) {
      throw new TypeError(`Invalid "config" value: ${config}.`)
    }
    if (!logger) {
      throw new TypeError(`Invalid "logger" value: ${logger}.`)
    }
    if (!client) {
      throw new TypeError(`Invalid "client" value: ${client}.`)
    }

    this.config = config
    this.log = logger
    this.client = client
  }

  getHttpClient() {
    const client = this.client.create({
      baseURL: this.config.apiUrl,
      timeout: 15 * 1000,
    })

    client.interceptors.response.use(
      (response) => response.data,
      (error) => {
        this.log.error(error)
        throw new Error(error)
      },
    )
    return client
  }
}

module.exports = HttpClient
