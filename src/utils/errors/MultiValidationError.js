const BaseError = require('./BaseError')

class MultiValidationError extends BaseError {
  constructor(id, errors) {
    super()
    this.id = id
    this.errors = errors
  }
}

module.exports = MultiValidationError
